EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BQ7692000PW:BQ7692000PW U?
U 1 1 5B79A674
P 2965 2230
F 0 "U?" H 2965 3197 50  0000 C CNN
F 1 "BQ7692000PW" H 2965 3106 50  0000 C CNN
F 2 "SOP65P640X120-20N" H 2965 2230 50  0001 L BNN
F 3 "3 to 5-Series Cell Li-Ion and Li-Phosphate Battery Monitor _bq76940 Family_ 20-TSSOP -40 to 85" H 2965 2230 50  0001 L BNN
F 4 "Bad" H 2965 2230 50  0001 L BNN "Field4"
F 5 "TSSOP-20 Texas Instruments" H 2965 2230 50  0001 L BNN "Field5"
F 6 "BQ7692000PW" H 2965 2230 50  0001 L BNN "Field6"
F 7 "2.21 USD" H 2965 2230 50  0001 L BNN "Field7"
F 8 "Texas Instruments" H 2965 2230 50  0001 L BNN "Field8"
	1    2965 2230
	1    0    0    -1  
$EndComp
Text GLabel 2265 2730 0    50   Input ~ 0
SDA
Text GLabel 2265 1530 0    50   Input ~ 0
SCL
$EndSCHEMATC
