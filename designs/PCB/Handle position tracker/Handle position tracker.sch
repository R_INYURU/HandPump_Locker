EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Handle position tracker"
Date ""
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATtiny:ATtiny45V-10SU U1
U 1 1 5B9FB8BB
P 3575 2560
F 0 "U1" H 3045 2606 50  0000 R CNN
F 1 "ATtiny45V-10SU" H 3045 2515 50  0000 R CNN
F 2 "Package_SO:SOIJ-8_5.3x5.3mm_P1.27mm" H 3575 2560 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 3575 2560 50  0001 C CNN
	1    3575 2560
	1    0    0    -1  
$EndComp
$Comp
L Connector:AVR-ISP-6 J1
U 1 1 5B9FBA22
P 3320 4175
F 0 "J1" H 3040 4271 50  0000 R CNN
F 1 "AVR-ISP-6" H 3040 4180 50  0000 R CNN
F 2 "" V 3070 4225 50  0001 C CNN
F 3 " ~" H 2045 3625 50  0001 C CNN
	1    3320 4175
	1    0    0    -1  
$EndComp
$Comp
L dk_Motion-Sensors-Accelerometers:ADXL343BCCZ U2
U 1 1 5B9FBD54
P 6765 2250
F 0 "U2" H 7506 2053 60  0000 L CNN
F 1 "ADXL343BCCZ" H 7506 1947 60  0000 L CNN
F 2 "digikey-footprints:LGA-14_3x5mm" H 6965 2450 60  0001 L CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/ADXL343.pdf" H 6965 2550 60  0001 L CNN
F 4 "ADXL343BCCZ-ND" H 6965 2650 60  0001 L CNN "Digi-Key_PN"
F 5 "ADXL343BCCZ" H 6965 2750 60  0001 L CNN "MPN"
F 6 "Sensors, Transducers" H 6965 2850 60  0001 L CNN "Category"
F 7 "Motion Sensors - Accelerometers" H 6965 2950 60  0001 L CNN "Family"
F 8 "http://www.analog.com/media/en/technical-documentation/data-sheets/ADXL343.pdf" H 6965 3050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/analog-devices-inc/ADXL343BCCZ/ADXL343BCCZ-ND/3542918" H 6965 3150 60  0001 L CNN "DK_Detail_Page"
F 10 "ACCEL 2-16G I2C/SPI 14LGA" H 6965 3250 60  0001 L CNN "Description"
F 11 "Analog Devices Inc." H 6965 3350 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6965 3450 60  0001 L CNN "Status"
	1    6765 2250
	1    0    0    -1  
$EndComp
Text GLabel 3720 3975 2    50   Input ~ 0
MISO
Text GLabel 3720 4075 2    50   Input ~ 0
MOSI
Text GLabel 3720 4175 2    50   Input ~ 0
SCK
Text GLabel 3720 4275 2    50   Input ~ 0
RST
$Comp
L power:VCC #PWR01
U 1 1 5B9FC001
P 3220 3675
F 0 "#PWR01" H 3220 3525 50  0001 C CNN
F 1 "VCC" H 3237 3848 50  0000 C CNN
F 2 "" H 3220 3675 50  0001 C CNN
F 3 "" H 3220 3675 50  0001 C CNN
	1    3220 3675
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5B9FC06B
P 3220 4575
F 0 "#PWR02" H 3220 4325 50  0001 C CNN
F 1 "GND" H 3225 4402 50  0000 C CNN
F 2 "" H 3220 4575 50  0001 C CNN
F 3 "" H 3220 4575 50  0001 C CNN
	1    3220 4575
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5B9FC0B0
P 3575 3160
F 0 "#PWR05" H 3575 2910 50  0001 C CNN
F 1 "GND" H 3580 2987 50  0000 C CNN
F 2 "" H 3575 3160 50  0001 C CNN
F 3 "" H 3575 3160 50  0001 C CNN
	1    3575 3160
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR04
U 1 1 5B9FC0DF
P 3575 1960
F 0 "#PWR04" H 3575 1810 50  0001 C CNN
F 1 "VCC" H 3592 2133 50  0000 C CNN
F 2 "" H 3575 1960 50  0001 C CNN
F 3 "" H 3575 1960 50  0001 C CNN
	1    3575 1960
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5B9FC16B
P 6965 3290
F 0 "#PWR07" H 6965 3040 50  0001 C CNN
F 1 "GND" H 6970 3117 50  0000 C CNN
F 2 "" H 6965 3290 50  0001 C CNN
F 3 "" H 6965 3290 50  0001 C CNN
	1    6965 3290
	1    0    0    -1  
$EndComp
Wire Wire Line
	6865 3150 6865 3210
Wire Wire Line
	6865 3210 6965 3210
Wire Wire Line
	7065 3210 7065 3150
Wire Wire Line
	6965 3150 6965 3210
Connection ~ 6965 3210
Wire Wire Line
	6965 3210 7065 3210
$Comp
L Diode:BZT52Bxx D1
U 1 1 5B9FC2FB
P 3505 5220
F 0 "D1" H 3505 5436 50  0000 C CNN
F 1 "BZT52Bxx" H 3505 5345 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 3505 5045 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bzt52b2v4.pdf" H 3505 5220 50  0001 C CNN
	1    3505 5220
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 5B9FC523
P 3255 5180
F 0 "#PWR03" H 3255 5030 50  0001 C CNN
F 1 "VCC" H 3272 5353 50  0000 C CNN
F 2 "" H 3255 5180 50  0001 C CNN
F 3 "" H 3255 5180 50  0001 C CNN
	1    3255 5180
	1    0    0    -1  
$EndComp
Wire Wire Line
	3355 5220 3255 5220
Wire Wire Line
	3255 5220 3255 5180
$Comp
L power:+3.3V #PWR06
U 1 1 5B9FC632
P 3770 5180
F 0 "#PWR06" H 3770 5030 50  0001 C CNN
F 1 "+3.3V" H 3785 5353 50  0000 C CNN
F 2 "" H 3770 5180 50  0001 C CNN
F 3 "" H 3770 5180 50  0001 C CNN
	1    3770 5180
	1    0    0    -1  
$EndComp
Wire Wire Line
	3770 5180 3770 5220
Wire Wire Line
	3770 5220 3655 5220
$Comp
L power:+3.3V #PWR08
U 1 1 5B9FC6E1
P 7015 1750
F 0 "#PWR08" H 7015 1600 50  0001 C CNN
F 1 "+3.3V" H 7030 1923 50  0000 C CNN
F 2 "" H 7015 1750 50  0001 C CNN
F 3 "" H 7015 1750 50  0001 C CNN
	1    7015 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6965 1850 6965 1800
Wire Wire Line
	6965 1800 7015 1800
Wire Wire Line
	7065 1800 7065 1850
Wire Wire Line
	7015 1750 7015 1800
Connection ~ 7015 1800
Wire Wire Line
	7015 1800 7065 1800
Wire Wire Line
	6965 3210 6965 3290
NoConn ~ 6165 2650
NoConn ~ 6165 2750
NoConn ~ 7465 2450
NoConn ~ 7465 2350
Text GLabel 6165 2450 0    50   Input ~ 0
SDA
Text GLabel 6165 2550 0    50   Input ~ 0
SCL
Text GLabel 4175 2360 2    50   Input ~ 0
MISO
Text GLabel 4175 2260 2    50   Input ~ 0
MOSI
Text GLabel 4175 2460 2    50   Input ~ 0
SCK
Text GLabel 4175 2760 2    50   Input ~ 0
RST
$EndSCHEMATC
