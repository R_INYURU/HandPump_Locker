EESchema Schematic File Version 4
LIBS:Flow meter-cache
EELAYER 26 0
EELAYER END
$Descr User 8661 5906
encoding utf-8
Sheet 1 1
Title "Flow meter counter"
Date "2018-09-12"
Rev "1.0"
Comp ""
Comment1 "This circuit combines both the optical and magnetic sensor which are selectable."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR01
U 1 1 5B997976
P 1815 2015
F 0 "#PWR01" H 1815 1765 50  0001 C CNN
F 1 "GND" H 1820 1842 50  0000 C CNN
F 2 "" H 1815 2015 50  0001 C CNN
F 3 "" H 1815 2015 50  0001 C CNN
	1    1815 2015
	1    0    0    -1  
$EndComp
$Comp
L MCU_Microchip_ATtiny:ATtiny45V-10SU U3
U 1 1 5B997CD5
P 4560 1770
F 0 "U3" H 4030 1816 50  0000 R CNN
F 1 "ATtiny45V-10SU" H 4030 1725 50  0000 R CNN
F 2 "Package_SO:SOIJ-8_5.3x5.3mm_P1.27mm" H 4560 1770 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 4560 1770 50  0001 C CNN
	1    4560 1770
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5B997E4C
P 4560 1170
F 0 "#PWR05" H 4560 1020 50  0001 C CNN
F 1 "VCC" H 4577 1343 50  0000 C CNN
F 2 "" H 4560 1170 50  0001 C CNN
F 3 "" H 4560 1170 50  0001 C CNN
	1    4560 1170
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5B997E7E
P 4560 2370
F 0 "#PWR06" H 4560 2120 50  0001 C CNN
F 1 "GND" H 4565 2197 50  0000 C CNN
F 2 "" H 4560 2370 50  0001 C CNN
F 3 "" H 4560 2370 50  0001 C CNN
	1    4560 2370
	1    0    0    -1  
$EndComp
Text GLabel 1815 1615 1    50   Input ~ 0
PHOTO-DARL
$Comp
L Connector:AVR-ISP-6 J1
U 1 1 5B997FBC
P 6660 1770
F 0 "J1" H 6380 1866 50  0000 R CNN
F 1 "AVR-ISP-6" H 6380 1775 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Horizontal" V 6410 1820 50  0001 C CNN
F 3 " ~" H 5385 1220 50  0001 C CNN
	1    6660 1770
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5B99806C
P 6560 2170
F 0 "#PWR08" H 6560 1920 50  0001 C CNN
F 1 "GND" H 6565 1997 50  0000 C CNN
F 2 "" H 6560 2170 50  0001 C CNN
F 3 "" H 6560 2170 50  0001 C CNN
	1    6560 2170
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR07
U 1 1 5B998083
P 6560 1270
F 0 "#PWR07" H 6560 1120 50  0001 C CNN
F 1 "VCC" H 6577 1443 50  0000 C CNN
F 2 "" H 6560 1270 50  0001 C CNN
F 3 "" H 6560 1270 50  0001 C CNN
	1    6560 1270
	1    0    0    -1  
$EndComp
Text GLabel 7060 1570 2    50   Input ~ 0
MISO
Text GLabel 7060 1670 2    50   Input ~ 0
MOSI
Text GLabel 7060 1770 2    50   Input ~ 0
SCK
Text GLabel 7060 1870 2    50   Input ~ 0
RST
Text GLabel 5160 1570 2    50   Input ~ 0
MISO
Text GLabel 5160 1470 2    50   Input ~ 0
MOSI
Text GLabel 5160 1670 2    50   Input ~ 0
SCK
Text GLabel 5460 1970 2    50   Input ~ 0
RST
Text GLabel 4280 3035 0    50   Input ~ 0
PHOTO-DARL
$Comp
L power:GND #PWR04
U 1 1 5B9987F0
P 2640 2015
F 0 "#PWR04" H 2640 1765 50  0001 C CNN
F 1 "GND" H 2645 1842 50  0000 C CNN
F 2 "" H 2640 2015 50  0001 C CNN
F 3 "" H 2640 2015 50  0001 C CNN
	1    2640 2015
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5B998922
P 3090 2015
F 0 "R1" V 2883 2015 50  0000 C CNN
F 1 "1K" V 2974 2015 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3020 2015 50  0001 C CNN
F 3 "~" H 3090 2015 50  0001 C CNN
	1    3090 2015
	0    1    1    0   
$EndComp
Text GLabel 3240 2015 2    50   Input ~ 0
IR-TX
Text GLabel 5160 1870 2    50   Input ~ 0
IR-TX
$Comp
L power:VCC #PWR02
U 1 1 5B9995DE
P 1975 2935
F 0 "#PWR02" H 1975 2785 50  0001 C CNN
F 1 "VCC" H 1992 3108 50  0000 C CNN
F 2 "" H 1975 2935 50  0001 C CNN
F 3 "" H 1975 2935 50  0001 C CNN
	1    1975 2935
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5B999613
P 1975 3735
F 0 "#PWR03" H 1975 3485 50  0001 C CNN
F 1 "GND" H 1980 3562 50  0000 C CNN
F 2 "" H 1975 3735 50  0001 C CNN
F 3 "" H 1975 3735 50  0001 C CNN
	1    1975 3735
	1    0    0    -1  
$EndComp
Text GLabel 5160 1770 2    50   Input ~ 0
OUT-SIGNAL
$Comp
L Device:Jumper JP2
U 1 1 5B999936
P 4580 3035
F 0 "JP2" H 4580 3299 50  0000 C CNN
F 1 "Jumper" H 4580 3208 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 4580 3035 50  0001 C CNN
F 3 "~" H 4580 3035 50  0001 C CNN
	1    4580 3035
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP1
U 1 1 5B99998B
P 4570 3405
F 0 "JP1" H 4570 3669 50  0000 C CNN
F 1 "Jumper" H 4570 3578 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 4570 3405 50  0001 C CNN
F 3 "~" H 4570 3405 50  0001 C CNN
	1    4570 3405
	1    0    0    -1  
$EndComp
Text GLabel 4270 3405 0    50   Input ~ 0
HALL-SIGNAL
Text GLabel 2675 3335 2    50   Input ~ 0
HALL-SIGNAL
Text GLabel 4880 3035 2    50   Input ~ 0
OUT-SIGNAL
Text GLabel 4870 3405 2    50   Input ~ 0
OUT-SIGNAL
$Comp
L Sensor_Magnetic:A1302KLHLT-T U2
U 1 1 5B99A5DA
P 2075 3335
F 0 "U2" H 1845 3381 50  0000 R CNN
F 1 "A1302KLHLT-T" H 1845 3290 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23W" H 2075 2985 50  0001 L CIN
F 3 "http://www.allegromicro.com/~/media/Files/Datasheets/A1301-2-Datasheet.ashx" H 1975 3335 50  0001 C CNN
	1    2075 3335
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5B9ACA36
P 5310 1970
F 0 "R3" V 5103 1970 50  0000 C CNN
F 1 "0R" V 5194 1970 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5240 1970 50  0001 C CNN
F 3 "~" H 5310 1970 50  0001 C CNN
	1    5310 1970
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 5B9ACBB0
P 2525 3335
F 0 "R2" V 2318 3335 50  0000 C CNN
F 1 "0R" V 2409 3335 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2455 3335 50  0001 C CNN
F 3 "~" H 2525 3335 50  0001 C CNN
	1    2525 3335
	0    1    1    0   
$EndComp
$Comp
L FabECI:OP580DA Q1
U 1 1 5BACFA68
P 1715 1815
F 0 "Q1" H 1906 1861 50  0000 L CNN
F 1 "OP580DA" H 1906 1770 50  0000 L CNN
F 2 "" H 1715 1815 50  0001 C CNN
F 3 "http://www.ttelectronics.com/sites/default/files/download-files/OP580DA.pdf" H 1715 1815 50  0001 C CNN
	1    1715 1815
	1    0    0    -1  
$EndComp
$Comp
L FabECI:HIR11-21C D1
U 1 1 5BAD494A
P 2790 2015
F 0 "D1" H 2790 2305 50  0000 C CNN
F 1 "HIR11-21C" H 2790 2214 50  0000 C CNN
F 2 "" H 2790 2015 50  0001 C CNN
F 3 "http://www.everlight.com/file/ProductFile/201407052100495806.pdf" H 2790 2015 50  0001 C CNN
	1    2790 2015
	1    0    0    -1  
$EndComp
$Comp
L MCU_Microchip_ATtiny:ATtiny44A-SSU U1
U 1 1 5BAD53AF
P 6720 3250
F 0 "U1" H 6190 3296 50  0000 R CNN
F 1 "ATtiny44A-SSU" H 6190 3205 50  0000 R CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6720 3250 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc8183.pdf" H 6720 3250 50  0001 C CNN
	1    6720 3250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
