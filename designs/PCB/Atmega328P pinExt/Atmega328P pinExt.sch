EESchema Schematic File Version 4
LIBS:Atmega328P pinExt-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:ATMEGA88-THIN IC1
U 1 1 5B92274E
P 5480 2715
F 0 "IC1" H 5530 3969 45  0000 C CNN
F 1 "ATMEGA88-THIN" H 5530 3885 45  0000 C CNN
F 2 "fab:fab-TQFP32-08THIN" H 5510 2865 20  0001 C CNN
F 3 "" H 5480 2715 60  0001 C CNN
	1    5480 2715
	1    0    0    -1  
$EndComp
Wire Wire Line
	4580 1915 4480 1915
Wire Wire Line
	4480 1915 4480 2015
Wire Wire Line
	4480 2315 4580 2315
Wire Wire Line
	4580 2115 4480 2115
Connection ~ 4480 2115
Wire Wire Line
	4480 2115 4480 2315
Wire Wire Line
	4580 2015 4480 2015
Connection ~ 4480 2015
Wire Wire Line
	4480 2015 4480 2115
Wire Wire Line
	4580 3715 4510 3715
Wire Wire Line
	4510 3715 4510 3815
Wire Wire Line
	4510 3915 4580 3915
Wire Wire Line
	4580 3815 4510 3815
Connection ~ 4510 3815
Wire Wire Line
	4510 3815 4510 3915
$Comp
L power:GND #PWR0101
U 1 1 5B922994
P 4510 4010
F 0 "#PWR0101" H 4510 3760 50  0001 C CNN
F 1 "GND" H 4515 3837 50  0000 C CNN
F 2 "" H 4510 4010 50  0001 C CNN
F 3 "" H 4510 4010 50  0001 C CNN
	1    4510 4010
	1    0    0    -1  
$EndComp
Wire Wire Line
	4510 4010 4510 3915
Connection ~ 4510 3915
$Comp
L power:VCC #PWR0102
U 1 1 5B922A85
P 4220 1825
F 0 "#PWR0102" H 4220 1675 50  0001 C CNN
F 1 "VCC" H 4237 1998 50  0000 C CNN
F 2 "" H 4220 1825 50  0001 C CNN
F 3 "" H 4220 1825 50  0001 C CNN
	1    4220 1825
	1    0    0    -1  
$EndComp
Text GLabel 4580 1715 0    50   Input ~ 0
RST
Wire Wire Line
	4220 1825 4220 1915
Wire Wire Line
	4220 1915 4480 1915
Connection ~ 4480 1915
Text GLabel 6480 1715 2    50   Input ~ 0
PC0
Text GLabel 6480 1815 2    50   Input ~ 0
PC1
Text GLabel 6480 1915 2    50   Input ~ 0
PC2
Text GLabel 6480 2015 2    50   Input ~ 0
PC3
Text GLabel 6480 2115 2    50   Input ~ 0
PC4
Text GLabel 6480 2215 2    50   Input ~ 0
PC5
Text GLabel 6480 2315 2    50   Input ~ 0
ADC6
Text GLabel 6480 2415 2    50   Input ~ 0
ADC7
Text GLabel 6480 2615 2    50   Input ~ 0
PD0
Text GLabel 6480 2715 2    50   Input ~ 0
PD1
Text GLabel 6480 2815 2    50   Input ~ 0
PD2
Text GLabel 6480 2915 2    50   Input ~ 0
PD3
Text GLabel 6480 3015 2    50   Input ~ 0
PD4
Text GLabel 6480 3115 2    50   Input ~ 0
PD5
Text GLabel 6480 3215 2    50   Input ~ 0
PD6
Text GLabel 6480 3315 2    50   Input ~ 0
PD7
Text GLabel 6480 3515 2    50   Input ~ 0
PB0
Text GLabel 6480 3615 2    50   Input ~ 0
PB1
Text GLabel 6480 3715 2    50   Input ~ 0
PB2
Text GLabel 6480 3815 2    50   Input ~ 0
PB3
Text GLabel 6480 3915 2    50   Input ~ 0
PB4
Text GLabel 6480 4015 2    50   Input ~ 0
PB5
Text GLabel 4580 2715 0    50   Input ~ 0
PB6
Text GLabel 4580 2915 0    50   Input ~ 0
PB7
$Comp
L Connector_Generic:Conn_01x08 J3
U 1 1 5B922F38
P 7610 2795
F 0 "J3" H 7690 2787 50  0000 L CNN
F 1 "Conn_01x08" H 7690 2696 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 7610 2795 50  0001 C CNN
F 3 "~" H 7610 2795 50  0001 C CNN
	1    7610 2795
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 5B923101
P 7595 4360
F 0 "J1" H 7675 4352 50  0000 L CNN
F 1 "Conn_01x08" H 7675 4261 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 7595 4360 50  0001 C CNN
F 3 "~" H 7595 4360 50  0001 C CNN
	1    7595 4360
	1    0    0    -1  
$EndComp
Text GLabel 7410 2495 0    50   Input ~ 0
PD5
Text GLabel 7410 2595 0    50   Input ~ 0
PD6
Text GLabel 7410 2695 0    50   Input ~ 0
PD7
Text GLabel 7410 2795 0    50   Input ~ 0
PB0
Text GLabel 7410 2895 0    50   Input ~ 0
PB1
Text GLabel 7410 2995 0    50   Input ~ 0
PB2
Text GLabel 7410 3095 0    50   Input ~ 0
PB3
Text GLabel 7410 3195 0    50   Input ~ 0
PB4
Text GLabel 7400 3370 0    50   Input ~ 0
PC1
Text GLabel 7400 3470 0    50   Input ~ 0
PC0
Text GLabel 7400 3570 0    50   Input ~ 0
ADC7
Text GLabel 7400 3770 0    50   Input ~ 0
ADC6
Text GLabel 7400 3870 0    50   Input ~ 0
PB5
Text GLabel 7395 4060 0    50   Input ~ 0
PD2
Text GLabel 7395 4160 0    50   Input ~ 0
PD1
Text GLabel 7395 4260 0    50   Input ~ 0
PD0
Text GLabel 7395 4360 0    50   Input ~ 0
RST
Text GLabel 7395 4460 0    50   Input ~ 0
PC5
Text GLabel 7395 4560 0    50   Input ~ 0
PC4
Text GLabel 7395 4660 0    50   Input ~ 0
PC3
Text GLabel 7395 4760 0    50   Input ~ 0
PC2
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5B9241F0
P 7610 1170
F 0 "J4" H 7690 1162 50  0000 L CNN
F 1 "Conn_01x02" H 7690 1071 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7610 1170 50  0001 C CNN
F 3 "~" H 7610 1170 50  0001 C CNN
	1    7610 1170
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 5B9243CF
P 7600 3770
F 0 "J6" H 7680 3762 50  0000 L CNN
F 1 "Conn_01x02" H 7680 3671 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7600 3770 50  0001 C CNN
F 3 "~" H 7600 3770 50  0001 C CNN
	1    7600 3770
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 5B9244D7
P 7610 1510
F 0 "J5" H 7690 1502 50  0000 L CNN
F 1 "Conn_01x04" H 7690 1411 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7610 1510 50  0001 C CNN
F 3 "~" H 7610 1510 50  0001 C CNN
	1    7610 1510
	1    0    0    -1  
$EndComp
Text GLabel 7410 2250 0    50   Input ~ 0
PB6
Text GLabel 7410 2350 0    50   Input ~ 0
PB7
Text GLabel 7410 2050 0    50   Input ~ 0
PD3
Text GLabel 7410 2150 0    50   Input ~ 0
PD4
$Comp
L power:VCC #PWR02
U 1 1 5B924952
P 7310 1105
F 0 "#PWR02" H 7310 955 50  0001 C CNN
F 1 "VCC" H 7327 1278 50  0000 C CNN
F 2 "" H 7310 1105 50  0001 C CNN
F 3 "" H 7310 1105 50  0001 C CNN
	1    7310 1105
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5B92499D
P 7290 1750
F 0 "#PWR01" H 7290 1500 50  0001 C CNN
F 1 "GND" H 7295 1577 50  0000 C CNN
F 2 "" H 7290 1750 50  0001 C CNN
F 3 "" H 7290 1750 50  0001 C CNN
	1    7290 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7290 1710 7410 1710
Wire Wire Line
	7410 1410 7290 1410
Wire Wire Line
	7290 1410 7290 1510
Connection ~ 7290 1710
Wire Wire Line
	7290 1710 7290 1750
Wire Wire Line
	7410 1610 7290 1610
Connection ~ 7290 1610
Wire Wire Line
	7290 1610 7290 1710
Wire Wire Line
	7410 1510 7290 1510
Connection ~ 7290 1510
Wire Wire Line
	7290 1510 7290 1610
Wire Wire Line
	7410 1270 7310 1270
Wire Wire Line
	7310 1270 7310 1170
Wire Wire Line
	7310 1170 7410 1170
Wire Wire Line
	7310 1170 7310 1105
Connection ~ 7310 1170
$Comp
L Connector:AVR-ISP-6 J8
U 1 1 5B928ABC
P 5550 5155
F 0 "J8" H 5270 5251 50  0000 R CNN
F 1 "AVR-ISP-6" H 5270 5160 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical_SMD" V 5300 5205 50  0001 C CNN
F 3 " ~" H 4275 4605 50  0001 C CNN
	1    5550 5155
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 5B928B92
P 5450 4655
F 0 "#PWR03" H 5450 4505 50  0001 C CNN
F 1 "VCC" H 5467 4828 50  0000 C CNN
F 2 "" H 5450 4655 50  0001 C CNN
F 3 "" H 5450 4655 50  0001 C CNN
	1    5450 4655
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5B928C6C
P 5450 5555
F 0 "#PWR04" H 5450 5305 50  0001 C CNN
F 1 "GND" H 5455 5382 50  0000 C CNN
F 2 "" H 5450 5555 50  0001 C CNN
F 3 "" H 5450 5555 50  0001 C CNN
	1    5450 5555
	1    0    0    -1  
$EndComp
Text GLabel 5950 5255 2    50   Input ~ 0
RST
Text GLabel 5950 5155 2    50   Input ~ 0
PB5
Text GLabel 5950 4955 2    50   Input ~ 0
PB4
Text GLabel 5950 5055 2    50   Input ~ 0
PB3
$Comp
L Connector_Generic:Conn_01x04 J7
U 1 1 5B929293
P 7610 2150
F 0 "J7" H 7690 2142 50  0000 L CNN
F 1 "Conn_01x04" H 7690 2051 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7610 2150 50  0001 C CNN
F 3 "~" H 7610 2150 50  0001 C CNN
	1    7610 2150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 5B9293C9
P 7600 3470
F 0 "J2" H 7680 3512 50  0000 L CNN
F 1 "Conn_01x03" H 7680 3421 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7600 3470 50  0001 C CNN
F 3 "~" H 7600 3470 50  0001 C CNN
	1    7600 3470
	1    0    0    -1  
$EndComp
$EndSCHEMATC
