# Table of content

[Project proposal](Project_proposal) - Project proposal for Aya Tsuboi
[Mechanical Design - Guide rails](Guide rails) - Designing a guide rails which will be connected with locking rod bar.
[Mechanical Design - Stepper motor gear](StepperMotorGear) - Designing a stepper motor gear for a belt.
[Battery](Battery)
[Electronic parts](Electronic parts) - Describe the electronic parts, their use and how they will connect together
[Battery charger](Battery charger) - Document battery charger chip and how to use it.
[Motor driver circuit](Motor driver circuit) - design of the stepper motor driver.
[Fluid flow measurement](Fluid flow measurement) - Science and types of fluid flow measurements used.
[Magnetic flow meter](Magnetic flow meter) - 
[Turbine flow meter](Turbine flow meter)

