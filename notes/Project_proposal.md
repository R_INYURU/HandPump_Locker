# Project proposal

## Problem 

In rural area, most people use boreholes hand pump to get clean and safe water. Then after sometime when the hand pump is damaged or fails it takes a longtime to fix the hand pump, in the meantime villagers uses dirty water.  

It takes a longtime to fix the hand pump because the money to fix it comes from the villagers who uses that hand pump. When its time to collect that money it takes long time, due to villagers who don't trust people who collects that money and they don't get a receipt.  

## Solution

Create a system which will help villagers to maintain the hand pump whenever is broken or fails.  
The system will act like a locker for the hand pump so that to be used the person must first pay for the service, That money will be used to maintain the hand pump.  

## Working principle

The system will lock the hand pump handle to stop the user to pump until they pay for the service.  
The user will pay using Mobile Money system, he/she will get a message with a token number to be used to unlock the system. The token will be entered on the control panel of the lock, if the token is valid the lock will retract and the user will start to pump and get the water.

## Control panel

The control panel will get the token from the user and send a request on the web application to check if is valid. If the token entered is valid the lock will be retracted and the user will start pumping.  
If the token entered by the user is not valid the lock will stay in locking mode.

## Web Application

The web application(web app) will be used to link the user to the mobile money system he/she use when paying for the service, the web app will generate a token to the user to be used on the control panel.  
The web app will keep data collected by the control panel and payments made for the service.  

### Material needed for control panel

The control panel will be composed with 2 parts.
* Mechanical part
* Electronic part

#### Mechanical part

This part will be in charge of moving the lock. The lock will be made of a metal bar which will be blocking the hand pump.  
The mechanical part will be in charge of moving that metal bar for locking or unlocking.  

**List of materials:**  

| No | Name                  | Size(cm) | Pieces | Unit price(Rwf) | Total Price(Rwf) |
|----|-----------------------|----------|--------|-----------------|------------------|
| 1  | Cylindrical metal bar | 20x3     | 6      | 3500            | 21000            |
| 2  | Metal sheet           | 50x50    | 6      | 2000            | 12000            |
| 3  | Ball bearings         |          | 2      | 3000            | 6000             |
| 4  | Bearing holder        | 10x1     | 6      | 2500            | 15000            |

**Note**: These prices are from the local market and they can only be considered for prototype not for final product.  

#### Electronic part

The electronic part will make request to the web application for checking the validity of the token entered by the user and it will provide the command to move the mechanical part.

**List of components:**  

| No | Name                 | Value      | Pieces | Unit price(USD) | Total price(USD) | Internet link |
|----|----------------------|------------|--------|-----------------|------------------|---------------|
| 1  | Microcontroller      | ATmega328p | 10     | 1.721 USD       | 17.21 USD        |               |
| 2  | GSM module           | SIM800L    | 10     | 8.485 USD       | 84.85 USD        |               |
| 3  | Stepper motor        | NEMA17     | 10     | 15.795 USD      | 157.95 USD       |               |
| 4  | LCD screen           | 16x2 LCD   | 10     | 2.048 USD       | 20.48 USD        |               |
| 5  | LED indicator        | NeoPixel   | 10     | 5.223 USD       | 52.23 USD        |               |
| 6  | Keypad               | 4x3        | 10     | 0.70 USD        | 7.80 USD         |               |
| 7  | Battery              | 3.7V       | 10     | 13.488 USD      | 134.88 USD       |               |
| 8  | Solar panel          | 5V - 4.5W  | 10     | 9.229 USD       | 92.29 USD        |               |
| 9  | Stepper motor divers | 6V         | 10     | 1.628 USD       | 16.28 USD        |               |
| 10 | Battery chager       | 3.7/4.2V   | 10     | 8.173 USD       | 81.73 USD        |               |

**Note**: These prices are from a website and they can change at anytime without being notified.  

