# Fluid flow measurement

**Flow measurement** is the quantification of bulk fluid movement.  
Flow can be measured in a variety of ways:  
- **Positive-displacement flow meters** accumulate a fixed volume of fluid and then count the number of times the volume is filled to measure flow.  
- Other methods rely on **forces** produced by the flowing stream as it overcomes konwn constriction, to indirectly calculate flow.  
- Flow may be measured by measuring the **velocity** of fluid over a known area.  
- For very large flows, **tracer methods** may be used to deduce the flow rate from the change in concentration if adye or radioisotope.  

## Mechanical flow meters

### Positive displacement meter

A positive displacement meter may be compared to a bucket and a stopwatch. The stopwatch is started when the flow starts, and stopped when the bucket reaches its limit. The volume divided by the time gives the flow rate. For continuous measurements, we need a system of continually filling and emptying buckets to divide the flow without letting it out of the pipe.  

### Piston meter/Rotary piston

The piston meter operates on the principle of a piston rotating within a chamber of known volume. For each rotation, an amount of water passes through the piston chamber. Through a gear mechanism and, sometimes, a magnetic drive, a needle dial and adometer type display are advanced.  

## Electronic flow meters

### Magnetic flow meters

Often called "mag meter"s or "electromag"s, use a magnetic field applied to the metering tube, which results in a potential difference proportional to the flow velocity perpendicular to the flux lines. The potential difference is sensed by electrodes aligned perpendicular to the flow and the applied magnetic field. The physical principle at work is Faraday's law of electromagnetic induction.  
The magnetic flow meter requires a conducting fluid and a nonconducting pipe liner.  

### Ultrasonic flow meters

