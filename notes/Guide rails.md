# Guide Rails

Are mechanism that guide the objects through a channel.  
In our case the are used to guide the locking rod bar.  

## Working Principle

Guide rails will transport/guide the locking bar, it will easy the movement of the locking bar. A belt will be connected to the rails to move it, the rails and the motor are the moving parts in the control panel.  

## Designing

I started by sketching down the design I had in mind, I designed the bottom view(BV) and the front view(FV).  
![Img: Sketch](./media/guide_rails/sketch.jpg)  

This guide rails is a moving part, is used to facilitate the motion. I added the ball bearings at the top as well as at the bottom. Those bearings will be used to smooth motion of the guide rails on a 90 degree angled bar.  
![Img: Sketch 2](./media/guide_rails/sketch-2.jpg)  

Starting designing the model I used FreeCAD software to design it.  
I started sketching a rectangle cube of 75x70x42mm (LxWxH), which will enclose every part of the guide rails.  
![Img: Cube rect](./media/guide_rails/rect_cube.png)  

I pocketed the holes of 30mm where screws that holed the locking bar will enter.  
![Img: Lock screw](./media/guide_rails/lock_bar_screw.png)  

I created a semi-circle of 11mm diameter where the locking bar will be hold tight.  
![Img: Lock bed](./media/guide_rails/lock_bar_bed.png)  

After that I created a hole/pocket where bearings will be placed/held when working. The hole/pocket is 24x18x18mm (LxWxH) and its on both sides the top and bottom.  
![Img: Top bearing pocket](./media/guide_rails/top_bearing_pocket.png)  
![Img: Bottom bearing pocket](./media/guide_rails/bottom_bearing_pocket.png)  

I inserted small pocktes on the left and right side of the guide rails where the belt will pass.  
![Img: Belt pocket](./media/guide_rails/belt_pocket.png)  

Finally I made holes which will used to insert holders for bearings.  
![Img: Bearing holder pocket](./media/guide_rails/bearing_holder_pocket.png)  

These are the image of the Front, Top, and Side view of the guide rails in **wireframe** draw style.  
![Img: Front view](./media/guide_rails/front_view.png)  
![Img: Top view](./media/guide_rails/top_view.png)  
![Img: Side view](./media/guide_rails/side_view.png)  

## Printing 

After designing, I exported the design file as **STL** opened it CURA software for printing.  
![Img: File in Cura](./media/guide_rails/file_in_cura.png)  

I used the recommended settings in CURA for printing which I enabled the support of the overhanging parts and the nozzle diameter was 0.4 mm and the material I used is PLA.  
![Img: Print settings 1](./media/guide_rails/print_settings-1.png)  
![Img: Print settings 2](./media/guide_rails/print_settings-2.png)  

The approximate time of the print was 9 hours but its according to the software estimation.  
![Img: Print time](./media/guide_rails/print_time.png)  

These are images I took in the middle of printing the part.  
![Img: Prints 1](./media/guide_rails/prints-1.jpg)  
![Img: Prints 2](./media/guide_rails/prints-2.jpg)  

After approximately 10 hours the print was over, this is how the printed part  locks like.  
![Img: Printed part](./media/guide_rails/printed_part-1.jpg)  
![Img: Printed part 2](./media/guide_rails/printed_part-2.jpg)  

## UPDATE

### Version 1.01

* After printing I test the belt slot if it fits and I made the length 10mm to the belt.  
* I made pockets which will help to lock the bearing holder in place.

![Img: sketching locker](./media/guide_rails/sketching_lock.png)  
![Img: locking slots](./media/guide_rails/locking_slots.png)  

### Version 1.02

* changed the size of the rectangle cube.
* Adjusted most part to fit new size.
