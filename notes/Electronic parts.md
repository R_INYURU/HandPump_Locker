# Electronic parts

Explain/Describe the electronic parts you will use and how the will work in the circuit.  

## Parts to use

| No | Name					| Value/Model		|
|----|----------------------|-------------------|
| 1  | Microcontroller(MCU)	| ATmega328P		|
| 2  | GSM module			| SIM800L			|
| 3  | Stepper motor		| NEMA 17			|
| 4  | Stepper motor driver | A4982				|
| 5  | LCD screen			| 16x2				|
| 6  | LED indicator		| ASMB-MTB0-0A3A2   |
| 7  | Keypad				| 4x3				|
| 8  | Battery				| Li-ion 18650		|
| 9  | Solar panel			| Mono crystalline  |
| 10 | Battery charger		| BQ76920			|
| 11 | Accelerometer		| ADXL343			|

These are electronic parts I will use for controlling the hand pump and communicating with the Web App.  

## Parts and their use

* **Micro controller**: This is the main processor/brain of the system, its where all other parts will be connected. This part will retain the instruction of each part needs to operate.  
* **GSM module**: For the system to communicate with the Web App it needs connectivity. This module will be used to connect the hand pump to the Web App.  
* **Stepper motor**: This is for moving the lock bar while closing or opening.  
* **Stepper motor**: This is for efficiently operate the motor on the write voltage.  
* **LCD screen**: This is an indicator for detailed information to the user.
* **LED indicator**: For quick notification, this will be used to indicate which mode the system is in.
* **Keypad**: This is for the user to interact with the system while inputting information.
* **Battery**: This part is for powering the whole system.
* **Solar panel**: This will be used to charger the battery.
* **Battery charger**: This part is for controlling/monitoring the battery during its usage.
* **Accelerometer**: This part is for tracking the position of the hander of the hand pump.

## Parts model description

### ATmega328P

Its an 8 bit AVR micro controller with 26 I/O pins, The package I am using is a 32-pin TQFP.  
![Img: chip pinout](./media/electronic_descr/capture1.png)

Chip features summary are in the below image.  
![Img: chip feature](./media/electronic_descr/capture2.png)

### SIM800L

SIM800L is a complete Quad-band GSM/GPRS module.  
SIM800L features GPRS multi-slot class 12/ class 10 (optional) and supports the GPRS coding schemes CS-1, CS-2, CS-3 and CS-4.  
SIM800L is designed with power saving technique so that the current consumption is as low as 0.7mA in sleep mode.  
![Img: SIM800L feature](./media/electronic_descr/capture3.png)  
![Img: SIM800L mode](./media/electronic_descr/capture4.png)

### NEMA 17 stepper motor

It is a 7VDC/350ma current per phase and 7.5 step angle.

### A4982 stepper motor driver

Is a complete microstepping motor driver with built-in translator for easy operation. It is designed to operate bipolar stepper motors in full-, half-, quarter-, and sixteenth-step modes.  
![Img: A4982 ratings](./media/electronic_descr/capture5.png)

### 16x2 display

This 16x2 display format with 32 characters of 5x8 dots, text color is black and operate at 4.7V ~ 5.5V supply.  
![Img: display characteristics](./media/electronic_descr/capture6.png)  

### ASMB-MTB0-0A3A2 LEDs

This is a common Anode white lens RGB LED.  
![Img: LED ratings](./media/electronic_descr/capture7.png)  
![Img: LED characteristics](./media/electronic_descr/capture8.png)

### 4x3 keypad

This keypad has 12 buttons, arranged in a telephone-line 3x4 grid. It's made of a thin, flexible membrane material with an adhesive backing (just remove the paper).  
The keys are connected into a matrix, so you only need 7 microcontroller pins (3-columns and 4-rows) to scan through the pad.

### Li-Ion 18650 battery

This is a cylindrical Lithium Ion Rechargeable cell.  
![Img: Battery characteristics](./media/electronic_descr/capture9.png)

### Mono crystalline solar panel

This is a 5V 1.25W 250mA solar panel. High conversion rate and high efficiency output.  
Its made in Monocrystalline silicon with a size of 110x70 mm.

### BQ76920 battery charger

It is a Battery monitor for Li-Ion and phosphate. It supports up to 5-series cells or typical 18-V packs.  
Through I<sup>2</sup>C, a host controller can use the bq76920 to implement many battery pack management functions, such as monitoring (cell voltages, pack current, pack temperatures), protection (controlling charge/discharge FETs), and balancing.  
Integrated A/D converters enable a purely digital readout of critical system parameters, with calibration handled in TI's manufacturing process.  
![Img: BQ76920 ratings](./media/electronic_descr/capture10.png)  
![Img: BQ76920 cell connection](./media/electronic_descr/capture11.png)  

### ADXL343 Accelerometer

It is a versatile 3-axis, digital-output, low g MEMS accelerometer. It has selectable measurement range and bandwidth, and configurable, built-in motion detection.  
It measures acceleration with high resolution (13-bit) measurement at up to ±16g. It measures both dynamic acceleration resulting from motion or shock and static acceleration, such as gravity, that allows the device to be used as a tilt sensor.  
I<sup>2</sup>C and SPI digital communications are available, in both cases it operates as a slave.  
![Img: ADXL343 ratings](./media/electronic_descr/capture12.png)  

## Circuits needed

* **Power supply circuit**: This circuit will provide different output voltage for different components.
* **Position sensing circuit**: This is the circuit will be sensing the hand pump handle position.
* **Battery charger circuit**: The circuit for charging the battery through solar panel.
* **Motor driver circuit**: This is the circuit that drives the stepper motor.
* **Network interface circuit**: This is the circuit will connect the hardware to the internet.
* **Main circuit**: This circuit contains the main chip/MCU which will control all parts/modules.

### Power supply circuit

In this system are different parts that operates at different voltage level, for that I need a circuit that will separate those voltage at a desired level for each part.  

#### Working voltage of different parts

| No | part                | working voltage |
|----|---------------------|-----------------|
| 1  | ATmega328P          | 1.8V ~ 5.5V     |
| 2  | SIM800L             | 3.4V ~ 4.4V     |
| 3  | Stepper motor       | 5V ~ 12V        |
| 4  | A4982 motor driver  | 5V ~ 35V        |
| 5  | 16x2 LCD            | 4.7V ~ 5.3V     |
| 6  | ASMB-MTB0-0A32 LEDs | 1.8V ~ 3.6V     |
| 7  | 4x3 Keypad          | 1.8V ~ 5.5V     |
| 8  | Battery charger     | -0.3V ~ 36V     |
| 9  | Accelerometer       | 2.0 ~ 3.6V      |

The table above will help in deciding different output voltage the power supply circuit will have.  
For simplification I can design a **3.3V**, **3.7V-4.2V**, **7.2V-11V**

| Voltage range | Parts               |
|---------------|---------------------|
| 3.3V          | ASMB-MTB0-0A32 LEDs |
| 3.3V          | Accelerometer       |
| 3.7V - 4.2V   | ATmega328P          |
| 3.7V - 4.2V   | SIM800L             |
| 3.7V - 4.2V   | 16x2 LCD            |
| 3.7V - 4.2V   | 4x3 Keypad          |
| 7.2V - 11V    | Stepper motor       |
| 7.2V - 11V    | A4982 motor driver  |
| 7.2V - 11V    | Battery charger     |

### Position sensing circuit

To accurately lock the hand pump without damaging its handle or any other part, we have to track the position of the handle in real time.  
For that I decided to use an accelerometer to track the position of the handle.  

The accelerometer will be attached to the handle and connected to the main controller using wires to make it move freely.  
The accelerometer will talk to the MCU using SPI/I<sup>2</sup>C.

### Battery charger circuit

Since I will be supplying power to the system using batteries, so I need a circuit to charge them.  I will use the solar panel to charge batteries.  
This circuit is for making the battery charging system efficient.  

### Motor driver circuit

Because we need a motor driver to drive the stepper motor, this circuit is for the chip that drives the stepper motor and different parts it needs.  

### Network interface circuit

As we will use SIM800L chip for connecting the whole system to the internet, we need a circuit to drive this chip.  
Because we will be using a module so this circuit is not difficult.  

### Main circuit 

This circuit is for the main MCU and other component I didn't talk about like keypad, LEDs, etc.  
Main circuit pin connection table:  

| No | Part name               | MCU interface  | Pin name                          |
|----|-------------------------|----------------|-----------------------------------|
| 1  | SIM800L                 | UART           | PD0,PD1                           |
| 2  | A4982 motor driver      | GPIO           | PD3(Step), PD4(Dir)               |
| 3  | 16x2 LCD                | I<sup>2</sup>C | PC4, PC5                          |
| 4  | RGB LEDs                | GPIO           | PD5, PD6, PB1                     |
| 5  | 4x3 Keypad              | GPIO           | PB0, PD7, PC0, PC1, PC2, PC3, PD2 |
| 6  | Accelerometer           | SPI            | PB3, PB4, PB5, PB2                |
| 7  | Battery charger BQ76920 | I<sup>2</sup>C | PC4, PC5                          |


