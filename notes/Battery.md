# Battery

charging and discharging rate of a battery are governed by C-rates. The capacity of a battery is commonly rated at 1C , meaning that a fully charged  battery rated at 1Ah  should provide 1A for an hour, The same battery discharging at 0.5C should provide 500mA for two hours, and at 2C it delivers 2A for 30 minutes. Losses at fast discharges reduce the discharge time and these losses also affect charge times.

A C-rate of 1C is also known as a one-hour discharge; 0.5C or C/2 is a two-hour discharge and 0.2C or C/5 is a 5-hour discharge.  

