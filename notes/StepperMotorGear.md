# Stepper motor Gear

This gear will be used to connect stepper motor to the belt, which will move the guide rails.  

I started with a sketch which used to make a revolution.  
![Img: sketch](./media/stepper_gear/capture.png)  

Then from the sketch I made a revolution.
![Img: Revolution](./media/stepper_gear/capture1.png)  

After I created gear teeth on the revolution I made by first sketching a small square on the top, and paded it with a dimension of 13mm. After I made a polar pattern of an angle of 360<sup>o</sup> and an occurences of 20, which distributed the gear tooth around the revolution I made.  
![Img: Gear teeth](./media/stepper_gear/capture2.png)  

Then I added on top a cylindrical cap to close the gear.  
![Img: Top close](./media/stepper_gear/capture3.png)  

To finish I added a small screw hole to use for fastening the gear to the motor rotor.  
![Img: Screw hole](./media/stepper_gear/capture4.png)
