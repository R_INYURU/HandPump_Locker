# Battery charger

For battery charging of the system I choose to use Bq76920 chip which can charger and balance from 3 to 5 Li-Ion battery.  
I am able to communicate with the chip through I<sup>2</sup>C to implement many battery pack management functions, such as **monitoring**(cell voltages, pack current, pack temperatures), **protection**(controlling charge/discharge FETs), and **balancing**.  