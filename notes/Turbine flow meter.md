# Turbine flow meter

Turbine flowmeters use the mechanical energy of the fluid to rotate a “pinwheel” (rotor) in the flow stream. Blades on the rotor are angled to transform energy from the flow stream into rotational energy. The rotor shaft spins on bearings. When the fluid moves faster, the rotor spins proportionally faster.  
Shaft rotation can be sensed mechanically or by detecting the movement of the blades. Blade movement is often detected magnetically, with each blade or embedded piece of metal generating a pulse.  
