#include <AccelStepper.h>

/*   Speed and Steps
 *   1000   =   6326
 *   1500   =   6338
 *   1800   =   6293
 *   2000   =   6296
 *   2200   =   6296
 *   2500   =   6297
 *   2800   =   6299
 *   3000   =   6298
 *   4000   =   6297
 *   5000   =   6301
 *   6000   =   6273
 *   10000  =   
 *   15000  =   6243
 *   20000  =   6238
*/

// define a stepper and the pins it will use
AccelStepper stepper(AccelStepper::DRIVER, 3, 4);
int HomeBtn = 9, AwayBtn = 10;
int pos = 3600;
long distance = 0;
bool dist_covered = true;
long Home = -1, Away = 1;
long maxDist = 0;
bool dataRx = false, motorMoved = false;
int initSpeed = 20000;

void setup() {
  Serial.begin(115200);
  pinMode(HomeBtn, INPUT_PULLUP);
  pinMode(AwayBtn, INPUT_PULLUP);
  stepper.setMaxSpeed(initSpeed);
  stepper.setAcceleration(initSpeed);

  while (digitalRead(HomeBtn)) {  // Let the motor detect the Home coordinates
    stepper.moveTo(Home);
    stepper.run();
    Home --;
  }
  stepper.setCurrentPosition(0);  // Set the current position of the motor to 0
  Serial.println("Position 0 set");
  stepper.setMaxSpeed(initSpeed);
  stepper.setAcceleration(initSpeed);

  while (digitalRead(AwayBtn)) {  // Let the motor detect the max distance it can cover
    stepper.moveTo(Away);
    stepper.run();
    Away ++;
  }
  stepper.stop();
  maxDist = stepper.currentPosition();
  stepper.setCurrentPosition(maxDist);
  stepper.moveTo(maxDist-100);
  stepper.run();
  Serial.print("The max position is: ");
  Serial.println(maxDist);
  stepper.setMaxSpeed(5000);
  stepper.setAcceleration(5000);
}

void loop() {
  while (Serial.available() > 0) {
    distance = Serial.parseInt();
    dist_covered = false;
    // check distance inserted is greater than 0 and less than 1500
    if ((distance < 0) || (distance > maxDist)) {
      Serial.print("Enter a distance to move greater than 0 and less than ");
      Serial.println(maxDist);
      dataRx = true;
    }
    // set the position the motor will move [moveTo()]
    else {
      if (dataRx == false) {
        Serial.print("Distance to move is ");
        Serial.println(distance);
        stepper.moveTo(distance);
        dataRx = true;
      }
    }
  }

  if ((distance >= 0) && (distance <= maxDist)) {
    // check if motor has reached the destination
    if (stepper.distanceToGo() != 0) {
//      if (digitalRead(AwayBtn) || digitalRead(HomeBtn))
//        stepper.stop();
//      else
        stepper.run();
    }

    if ((dist_covered == false) && (stepper.distanceToGo() == 0)) {
      // indicate that the motor has covered all its distance
      Serial.println("Distance to go covered.");
      dist_covered = true;
      dataRx = false;

    }
  }

}

