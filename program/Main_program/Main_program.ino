#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>
#include <AccelStepper.h>

#define BLUE_LED  5
#define GREEN_LED 6
#define RED_LED   9

// Set the LCD address to 0x3F for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x3F, 16, 2);

// setting keypad
const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
char keys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};
byte rowPins[ROWS] = {8, 7, A0, A1}; // Rows pinout
byte colPins[COLS] = {A2, A3, 2}; // Colums pinout
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );
// Setting Motor driver
AccelStepper stepper(AccelStepper::DRIVER, 3, 4);
int HomeBtn = 9, AwayBtn = 10;    // PB6, PB7
long Home = 0, Away = 0;
long maxPos = 6300;   // Max position the motor can reach

int token[10] = {0};    // Holds the token number entered by the user
int newToken[10] = {0}; // Holds the token number entered -1 digit
int tknDig = 0;          // Counts number of token digits entered
unsigned long timer_t = 0;  // Tracks the time passed to clear the prompt
unsigned long timePassed = 0;   // Tracks time of pumping if is over
int secInc = 0;   // Counts the time spent pumping
unsigned long timeCount = 0;
bool retractMotor = false;

enum state {  // states the machine will operate in
  HOME_PAGE,  // This is the home state of the machine
  READ_KEY,   // Reading Keypad state
  FETCHING,   // Loading information from the server
  MOVE_MOTOR,  // Move the locker either Retracting it or Pushing it.
  CLOCK_COUNTING,   // Counting time passed while pumping water
} states;

void setup() {
  lcd.begin();  // initialising LCD
  lcd.backlight();  // Turn on the blacklight and print a message.

  keypad.setDebounceTime(1);
  keypad.addEventListener(keypadEvent);

  stepper.setMaxSpeed(20000);
  stepper.setAcceleration(20000);
  pinMode(AwayBtn, INPUT_PULLUP);
  pinMode(HomeBtn, INPUT_PULLUP);

  pinMode(BLUE_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);

  digitalWrite(BLUE_LED, LOW);
  digitalWrite(RED_LED, LOW);
  digitalWrite(GREEN_LED, LOW);
  delay(1000);
  digitalWrite(BLUE_LED, HIGH);
  digitalWrite(RED_LED, HIGH);
  digitalWrite(GREEN_LED, HIGH);
}

void loop() {
  char key;
  switch (states) {
    case HOME_PAGE:
      homePage();
      key = keypad.getKey();
      if (key) {
        if (key != '*' && key != '#') {
          states = READ_KEY;
          token[tknDig] = key;
          tknDig++;
          lcd.clear();
          lcd.print("Umubare ufungura");
          lcd.setCursor(0, 1);
          lcd.print(key);
        }
      }
      break;

    case READ_KEY:
      readKey();
      break;

    case FETCHING:  // Request verification of the code/token from the server
      fetching();
      break;
    // Retract or Push the locker
    case MOVE_MOTOR:
      // if motor is at HOME
      if (retractMotor) {
        // Signal the user for locker closing
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Igihe cyo kuvoma");
        lcd.setCursor(0, 1);
        lcd.print("cyawe cyarangiye");
        moveMotorFrd();   // Push the locker to close the pump
        states = HOME_PAGE;
        lcd.clear();
      }
      // otherwise is AWAY
      else {
        // Signal the user to start Pumping water
        moveMotorBck();   // Rectract the locker to open the pump
        lcd.clear();
        lcd.setCursor(3, 0);
        lcd.print("Mwatangira");
        lcd.setCursor(5, 1);
        lcd.print("ku voma");
        states = CLOCK_COUNTING;
        delay(1000);
        lcd.clear();
        timePassed = millis();
      }
      break;
    // Count time passed user pumping
    case CLOCK_COUNTING:
      pumpTime(30);   // Count the time passed since locker open
      readKey();
      break;
  }
}

void homePage() {
  lcd.setCursor(0, 0);
  lcd.print("Shyiramo umubare");
  lcd.setCursor(2 , 1);
  lcd.print("wo gufungura");
}

void readKey() {
  char key = keypad.getKey();

  if (key) { // Key pressed
    if (key != '*' && key != '#') {
      if (tknDig <= 7) { // check if 8 digits has been entered.
        token[tknDig] = key;
        tknDig++;
        lcd.print(key);
      }
    }
  }
  if ((millis() - timer_t) > 2000) { // clean the prompt of incomplete token
    if (states == READ_KEY){
      lcd.setCursor(0, 0);
      lcd.print("Umubare ufungura");
      lcd.setCursor(tknDig, 1);
    }
  }
}

void keypadEvent(KeypadEvent key) {
  switch (keypad.getState()) {
    case PRESSED:
      if (key == '#') {
        if (tknDig < 8) {  // Prompt the user to enter the complete token
          timer_t = millis();
          lcd.setCursor(0, 0);
          lcd.print("Andika imibare 8");
          lcd.setCursor(tknDig, 1);
        }
        else {  // Submit the token for verification
          states = FETCHING;
          // Clear token array and index counter
          tknDig = 0;
          token[10] = {0};
        }
      }
      else if (key == '*'){
        if (states == CLOCK_COUNTING){
          secInc = 0;   // clear the time counter
          states = MOVE_MOTOR;
          retractMotor = true;
        }
      }
      break;

    case  RELEASED:
      if (key == '*') {
        int i;
        for (i = 0; i <= 9; i++) {
          newToken[i] = token[i]; // Copy content of token to newToken
          token[i] = 0; // Delete the copied content of token
          lcd.setCursor(i, 1);
//          lcd.print(" ");
        }
        lcd.clear();
        lcd.setCursor(0, 1);
        for (i = 0; i <= tknDig - 2; i++) {
          token[i] = newToken[i]; // Rewrite back content of token from newToken
          lcd.print(char(token[i]));
        }
        newToken[10] = {0};
        if (tknDig > 0)
          tknDig--;
        else {
          tknDig = 0;
          token[10] = {0};
          states = HOME_PAGE;
        }
      }
      break;
  }
}

void fetching() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Uba wihanganye");
  lcd.setCursor(0, 1);
  lcd.print("gato ...");
  delay(5000);
  states = MOVE_MOTOR;
  retractMotor = false;
}

void pumpTime(unsigned long Time) {
  int minPassed = 0;
  int secPassed = 0;
  
  if ((millis() - timePassed) < (Time * 1000)) {    // there is still some time for pumping
    //display the time passed on screen
    if ((millis() - timeCount) > 1000) {
      timeCount = millis();
      secInc++;
//      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print("Igihe umaze");
      lcd.setCursor(5, 1);
      secPassed = secInc % 60;  // Calculating Seconds passed
      minPassed = secInc/60;
      if (secPassed > 0) {    // seconds pass are greater than one
        if (minPassed < 10)  lcd.print("0");
        lcd.print(minPassed);
        lcd.print(":");
      }
      else{   // Print minute passed
        if (minPassed < 10)  lcd.print("0");
        lcd.print(minPassed);
        lcd.print(":");
      }
      if (secPassed < 10) lcd.print("0");
      lcd.print(secInc % (60 * (minPassed)));   // display Seconds passed
    }
  }
  else {
    secInc = 0;   // clear the time counter
    states = MOVE_MOTOR;
    retractMotor = true;
  }
}
// Move forward/Push the locker
void moveMotorFrd() {
  while (digitalRead(AwayBtn)) {
    stepper.moveTo(Away);
    stepper.run();
    Away++;
  }
  stepper.stop();
  if (stepper.currentPosition() > 0) {
    stepper.moveTo(stepper.currentPosition() - 100);  // move back the locker 100 steps
    stepper.run();
  }
  else if (stepper.currentPosition() < 0) {
    stepper.moveTo(stepper.currentPosition() + 100);
    stepper.run();
  }
}
// Move back/Retract the locker
void moveMotorBck() {
  while (digitalRead(HomeBtn)) {
    stepper.moveTo(Home);
    stepper.run();
    Home--;
  }
  stepper.setCurrentPosition(0);    // Set position to Zero(0)
  stepper.moveTo(stepper.currentPosition() + 50);   // Move the locker in 50 steps
  stepper.run();
}

