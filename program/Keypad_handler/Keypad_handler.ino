#include <Keypad.h>

const byte ROWS = 4;
const byte COLS = 3;
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};
byte rowPins[ROWS] = {8, 7, A0, A1};
byte colPins[COLS] = {A2, A3, 2};

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

int token[10] = {0};    // Holds the token number entered by the user
int newToken[10] = {0}; // Holds the token number entered -1 digit
int tknDig = 0;          // Counts number of token digits entered 

void setup() {
  Serial.begin(115200);
  keypad.setDebounceTime(1);
  keypad.addEventListener(keypadEvent);
}

void loop() {
  char key = keypad.getKey();

  if (key){
    if (key != '*' && key != '#'){
      if (tknDig <= 7){    // Receive token if total digits are less than 8
        token[tknDig] = key;
        tknDig++;
        Serial.print(key);
        // TO DO: handle how to concantanate token on LCD when a user try to submit a portio of it 
      }
    }
  }
}

void keypadEvent(KeypadEvent key){
  switch (keypad.getState()){
    case PRESSED:
      if(key == '#'){
        Serial.println();
        if (tknDig <8){
          Serial.println("8 digits are needed for a complete token");
        }
        else {
          Serial.print("Token number entered is: ");
          int i;
          for (i=0; i<=9; i++){
            Serial.print(char(token[i]));
          }
          Serial.println();
          tknDig = 0;   // Initialise the token digit counter to receive token again
          token[10] = {0};  // Clear the token holder array
        }
      }
    break;

    case  RELEASED:
      if (key == '*'){
        int i;
        Serial.println();
        Serial.println("Deleting a digit from token");
        for (i=0; i<=9; i++){
          newToken[i] = token[i]; // Copy content of token to newToken
//          Serial.print(char(newToken[i]));
          token[i] = 0; // Delete the copied conted of token
        }
        for (i=0; i<=tknDig-2; i++){
          token[i] = newToken[i]; // Rewrite back content of token from newToken
          Serial.print(char(token[i]));
        }
        newToken[10] = {0};
        tknDig--;
      }
    break;
  }
}

